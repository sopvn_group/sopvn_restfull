﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class ProjectHrController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(ProjectHr item)
        {
            var service = new PMServiceClient();
            var result = service.NewProjectEmployee(item);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<ProjectHr> items)
        {
            var service = new PMServiceClient();
            var result = service.NewProjectEmployees(items);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(ProjectHr item)
        {
            var service = new PMServiceClient();
            var result = service.UpdateProjectEmployee(item);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<ProjectHr> items)
        {
            var service = new PMServiceClient();
            var result = service.UpdateProjectEmployees(items);

            return Json(result);
        }
        [HttpPost]
        [Route("ChangeStatus")]
        public JsonResult ChangeStatus(ProjectHr item)
        {
            var service = new PMServiceClient();
            var result = service.ChangeProjectEmployeeStatus(item);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<ProjectHr> items)
        {
            var service = new PMServiceClient();
            var result = service.DeleteProjectEmployees(items);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(ProjectHr item)
        {
            var service = new PMServiceClient();
            var result = service.GetProjectEmployee(item);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(ProjectHrDTO item)
        {
            var service = new PMServiceClient();
            var result = service.GetProjectEmployees(item);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(ProjectHr item)
        {
            var service = new PMServiceClient();
            var result = service.IsExsitsProjectEmployee(item);

            return Json(result);
        }
    }
}