﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class PermisionController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(Permision item)
        {
            var service = new PMServiceClient();
            var result = service.NewPermision(item);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<Permision> items)
        {
            var service = new PMServiceClient();
            var result = service.NewPermisions(items);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(Permision item)
        {
            var service = new PMServiceClient();
            var result = service.UpdatePermision(item);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<Permision> items)
        {
            var service = new PMServiceClient();
            var result = service.UpdatePermisions(items);

            return Json(result);
        }
        [HttpPost]
        [Route("ChangeStatus")]
        public JsonResult ChangeStatus(Permision item)
        {
            var service = new PMServiceClient();
            var result = service.ChangePermisionStatus(item);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<Permision> items)
        {
            var service = new PMServiceClient();
            var result = service.DeletePermisions(items);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(Permision item)
        {
            var service = new PMServiceClient();
            var result = service.GetPermision(item);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(Permision item)
        {
            var service = new PMServiceClient();
            var result = service.GetPermisions(item);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(Permision item)
        {
            var service = new PMServiceClient();
            var result = service.IsExistsPermision(item);

            return Json(result);
        }
    }
}