﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class PositionController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(Position data)
        {
            var service = new PMServiceClient();
            var result = service.NewPosition(data);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<Position> data)
        {
            var service = new PMServiceClient();
            var result = service.NewPositions(data);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(Position data)
        {
            var service = new PMServiceClient();
            var result = service.UpdatePosition(data);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<Position> data)
        {
            var service = new PMServiceClient();
            var result = service.UpdatePositions(data);

            return Json(result);
        }
        [HttpPost]
        [Route("ChangeStatus")]
        public JsonResult ChangeStatus(Position data)
        {
            var service = new PMServiceClient();
            var result = service.ChangePositionStatus(data);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<Position> data)
        {
            var service = new PMServiceClient();
            var result = service.DeletePositions(data);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(Position id)
        {
            var service = new PMServiceClient();
            var result = service.GetPosition(id);

            return Json(result);
        }
        [HttpGet]
        [Route("Position/Gets")]
        public JsonResult Gets(Position data)
        {
            var service = new PMServiceClient();
            var result = service.GetPositions(data);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(Position item)
        {
            var service = new PMServiceClient();
            var result = service.IsExistsPosition(item);

            return Json(result);
        }
    }
}
