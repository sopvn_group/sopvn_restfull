﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class UserStoryController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(UserStory item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.NewUserStory(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<UserStory> items, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.NewUserStories(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(UserStory item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.UpdateUserStory(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<UserStory> items, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.UpdateUserStories(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<UserStory> items, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.DeleteUserStories(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(UserStory item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.GetUserStory(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(UserStory item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.GetUserStories(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(UserStory item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.IsExistsUserStory(item, dbName);

            return Json(result);
        }
    }
}