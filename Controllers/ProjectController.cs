﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class ProjectController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(Project item)
        {
            var service = new PMServiceClient();
            var result = service.NewProject(item);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<Project> items)
        {
            var service = new PMServiceClient();
            var result = service.NewProjects(items);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(Project item)
        {
            var service = new PMServiceClient();
            var result = service.UpdateProject(item);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<Project> items)
        {
            var service = new PMServiceClient();
            var result = service.UpdateProjects(items);

            return Json(result);
        }
        [HttpPost]
        [Route("ChangeStatus")]
        public JsonResult ChangeStatus(Project item)
        {
            var service = new PMServiceClient();
            var result = service.ChangeProjectStatus(item);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<Project> items)
        {
            var service = new PMServiceClient();
            var result = service.DeleteProjects(items);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(Project item)
        {
            var service = new PMServiceClient();
            var result = service.GetProject(item);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(Project item)
        {
            var service = new PMServiceClient();
            var result = service.GetProjects(item);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(Project item)
        {
            var service = new PMServiceClient();
            var result = service.IsExistsProject(item);

            return Json(result);
        }
    }
}