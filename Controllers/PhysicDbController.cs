﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class PhysicDbController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(PhysicDatabase item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.NewPhysicDb(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<PhysicDatabase> item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.NewPhysicDbs(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(PhysicDatabase item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.UpdatePhysicDb(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<PhysicDatabase> item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.UpdatePhysicDbs(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<PhysicDatabase> item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.DeletePhysicDbs(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(PhysicDatabase item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.GetPhysicDb(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(PhysicDatabase item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.GetPhysicDbs(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(PhysicDatabase item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.IsExistsPhysicDb(item, dbName);

            return Json(result);
        }
    }
}