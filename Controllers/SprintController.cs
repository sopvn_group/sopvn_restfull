﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class SprintController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(Sprint item, string dbName)
        {
            var _SprintSc = new PMServiceClient();
            var result = _SprintSc.NewSprint(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<Sprint> items, string dbName)
        {
            var _SprintSc = new PMServiceClient();
            var result = _SprintSc.NewSprints(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(Sprint item, string dbName)
        {
            var _SprintSc = new PMServiceClient();
            var result = _SprintSc.UpdateSprint(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<Sprint> items, string dbName)
        {
            var _SprintSc = new PMServiceClient();
            var result = _SprintSc.UpdateSprints(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<Sprint> items, string dbName)
        {
            var _SprintSc = new PMServiceClient();
            var result = _SprintSc.DeleteSprints(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(Sprint item, string dbName)
        {
            var _SprintSc = new PMServiceClient();
            var result = _SprintSc.GetSprint(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(Sprint item, string dbName)
        {
            var _SprintSc = new PMServiceClient();
            var result = _SprintSc.GetSprints(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(Sprint item, string dbName)
        {
            var _SprintSc = new PMServiceClient();
            var result = _SprintSc.IsExistsSprint(item, dbName);

            return Json(result);
        }
    }
}