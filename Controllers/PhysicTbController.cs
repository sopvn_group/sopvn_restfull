﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class PhysicTbController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(PhysicTable item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.NewPhysicTable(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<PhysicTable> items, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.NewPhysicTables(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(PhysicTable item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.UpdatePhysicTable(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<PhysicTable> items, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.UpdatePhysicTables(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<PhysicTable> items, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.DeletePhysicTables(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(PhysicTable item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.GetPhysicTable(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(PhysicTable item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.GetPhysicTables(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(PhysicTable item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.IsExistsPhysicTable(item, dbName);

            return Json(result);
        }
    }
}