﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class SprintTestController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(SprintTest item, string dbName)
        {
            var _SprintTestSc = new PMServiceClient();
            var result = _SprintTestSc.NewSprintTest(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<SprintTest> items, string dbName)
        {
            var _SprintTestSc = new PMServiceClient();
            var result = _SprintTestSc.NewSprintTests(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(SprintTest item, string dbName)
        {
            var _SprintTestSc = new PMServiceClient();
            var result = _SprintTestSc.UpdateSprintTest(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<SprintTest> items, string dbName)
        {
            var _SprintTestSc = new PMServiceClient();
            var result = _SprintTestSc.UpdateSprintTests(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<SprintTest> items, string dbName)
        {
            var _SprintTestSc = new PMServiceClient();
            var result = _SprintTestSc.DeleteSprintTests(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(SprintTest item, string dbName)
        {
            var _SprintTestSc = new PMServiceClient();
            var result = _SprintTestSc.GetSprintTest(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(SprintTestDTO item, string dbName)
        {
            var _SprintTestSc = new PMServiceClient();
            var result = _SprintTestSc.GetSprintTests(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(SprintTest item, string dbName)
        {
            var _SprintTestSc = new PMServiceClient();
            var result = _SprintTestSc.IsExistsSprintTest(item, dbName);

            return Json(result);
        }
    }
}