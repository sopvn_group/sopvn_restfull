﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class RequirementController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(Requirement item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.NewRequirement(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<Requirement> items, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.NewRequirements(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(Requirement item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.UpdateRequirement(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<Requirement> items, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.UpdateRequirements(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<Requirement> items, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.DeleteRequirements(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(Requirement item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.GetRequirement(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(Requirement item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.GetRequirements(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(Requirement item, string dbName)
        {
            var service = new PMServiceClient();
            var result = service.IsExistsRequirement(item, dbName);

            return Json(result);
        }
    }
}