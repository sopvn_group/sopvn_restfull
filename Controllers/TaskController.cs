﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SopVnApi.Controllers
{
    public class TaskController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(Task item, string dbName)
        {
            var _TaskSc = new PMServiceClient();
            var result = _TaskSc.NewTask(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<Task> item, string dbName)
        {
            var _TaskSc = new PMServiceClient();
            var result = _TaskSc.NewTasks(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(Task item, string dbName)
        {
            var _TaskSc = new PMServiceClient();
            var result = _TaskSc.UpdateTask(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<Task> item, string dbName)
        {
            var _TaskSc = new PMServiceClient();
            var result = _TaskSc.UpdateTasks(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<Task> items, string dbName)
        {
            var _TaskSc = new PMServiceClient();
            var result = _TaskSc.DeleteTasks(items, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(Task item, string dbName)
        {
            var _TaskSc = new PMServiceClient();
            var result = _TaskSc.GetTask(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(TaskDTO item, string dbName)
        {
            var _TaskSc = new PMServiceClient();
            var result = _TaskSc.GetTasks(item, dbName);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(Task item, string dbName)
        {
            var _TaskSc = new PMServiceClient();
            var result = _TaskSc.IsExistsTask(item, dbName);

            return Json(result);
        }
    }
}