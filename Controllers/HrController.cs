﻿using SopVnApi.PMService;
using System.Collections.Generic;
using System.Web.Mvc;


namespace SopVnApi.Controllers
{
    public class HrController : Controller
    {
        [HttpPost]
        [Route("New")]
        public JsonResult New(Employee item)
        {
            var service = new PMServiceClient();
            var result = service.NewHr(item);

            return Json(result);
        }
        [HttpPost]
        [Route("NewMany")]
        public JsonResult NewMany(List<Employee> items)
        {
            var service = new PMServiceClient();
            var result = service.NewHrs(items);

            return Json(result);
        }
        [HttpPost]
        [Route("Update")]
        public JsonResult Update(Employee item)
        {
            var service = new PMServiceClient();
            var result = service.UpdateHr(item);

            return Json(result);
        }
        [HttpPost]
        [Route("UpdateMany")]
        public JsonResult UpdateMany(List<Employee> items)
        {
            var service = new PMServiceClient();
            var result = service.UpdateHrs(items);

            return Json(result);
        }
        [HttpPost]
        [Route("ChangeStatus")]
        public JsonResult ChangeStatus(Employee item)
        {
            var service = new PMServiceClient();
            var result = service.ChangeHrStatus(item);

            return Json(result);
        }
        [HttpPost]
        [Route("DeleteMany")]
        public JsonResult DeleteMany(List<Employee> items)
        {
            var service = new PMServiceClient();
            var result = service.DeleteHrs(items);

            return Json(result);
        }
        [HttpPost]
        [Route("Get")]
        public JsonResult Get(Employee item)
        {
            var service = new PMServiceClient();
            var result = service.GetHr(item);

            return Json(result);
        }
        [HttpPost]
        [Route("Gets")]
        public JsonResult Gets(Employee item)
        {
            var service = new PMServiceClient();
            var result = service.GetHrs(item);

            return Json(result);
        }
        [HttpPost]
        [Route("IsExists")]
        public JsonResult IsExists(Employee item)
        {
            var service = new PMServiceClient();
            var result = service.IsExistsHr(item);

            return Json(result);
        }
    }
}